<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');



Route::group(['middleware' => ['auth'], 'prefix' => 'admin/', 'as' => 'admin.', 'namespace' => 'Admin\\'], function () {

    Route::get('profile',           ['as' => 'profile',        'uses' => 'UserController@profile']);
    Route::post('profile',          ['as' => 'profile.update', 'uses' => 'UserController@profileUpdate']);

    Route::get('dashboard',         ['as' => 'dashboard',      'uses' => 'DashboardController@index']);

    //Route::resource('user','userController');
    Route::get('user',              ['as' => 'user',            'uses' => 'UserController@index']);
    Route::get('user/show/{id}',    ['as' => 'user.show',       'uses' => 'UserController@show']);
    Route::get('user/create',       ['as' => 'user.create',     'uses' => 'UserController@create']);
    Route::post('user/store',       ['as' => 'user.store',      'uses' => 'UserController@store']);
    Route::get('user/edit/{id}',    ['as' => 'user.edit',       'uses' => 'UserController@edit']);
    Route::post('user/update/{id}', ['as' => 'user.update',     'uses' => 'UserController@update']);
    Route::get('user/delete/{id}',  ['as' => 'user.delete',     'uses' => 'UserController@destroy']);

});
