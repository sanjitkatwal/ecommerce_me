@extends('admin.layouts.app')
@section('page_titel')
    Profile
@endsection



@section('content')

    <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
            <ul class="breadcrumb">
                @include('admin.includes.dashboard_link')

                <li>
                    <a href="{{ route('admin.user') }}">User</a>
                </li>
                <li class="active">List</li>
            </ul><!-- /.breadcrumb -->
        </div>

        <div class="page-content">

            <div class="page-header">
                <h1>
                    User Manager
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        List
                    </small>
                </h1>
            </div><!-- /.page-header -->

            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    <div class="row">
                        <div class="col-xs-12">
                            <table id="simple-table" class="table  table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th class="center">
                                        <label class="pos-rel">
                                            <input type="checkbox" class="ace">
                                            <span class="lbl"></span>
                                        </label>
                                    </th>
                                    <th class="detail-col">Email</th>
                                    <th>Name</th>
                                    <th class="hidden-480">Created At</th>
                                    <th>Action</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($data['rows'] as $row)
                                    <tr>
                                        <td class="center">
                                            <label class="pos-rel">
                                                <input type="checkbox" class="ace">
                                                <span class="lbl"></span>
                                            </label>
                                        </td>
                                        <td>
                                            <a href="#">{!! $row->email !!}</a>
                                        </td>
                                        <td>{!! $row->first_name.' '.$row->middle_name.' '.$row->last_name !!}</td>
                                        <td>{!! $row->created_at->diffForHumans(); !!}</td>

                                        <td>
                                            <div class="hidden-sm hidden-xs btn-group">
                                                <a href="{{ route('admin.user.show',$row->id) }}" class="btn btn-xs btn-success">
                                                    <i class="ace-icon fa fa-eye bigger-120"></i>
                                                </a>

                                                <button class="btn btn-xs btn-info">
                                                    <i class="ace-icon fa fa-pencil bigger-120"></i>
                                                </button>

                                                <button class="btn btn-xs btn-danger">
                                                    <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                                </button>

                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                <tr>
                                   <td colspan="5">{!! $data['rows']->links() !!}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div><!-- /.span -->
                    </div><!-- /.row -->

                    <div class="hr hr-18 dotted hr-double"></div>

                    <!-- PAGE CONTENT ENDS -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>

    @endsection
