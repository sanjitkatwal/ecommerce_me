<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use View;

class AdminBaseController extends Controller
{
    protected function loadCommonDataToView($view_path)
    {
        View::composer($view_path, function ($view){
            $view->with('dashboard_url', route('admin.dashboard'));
        });

        return $view_path;

    }
}
